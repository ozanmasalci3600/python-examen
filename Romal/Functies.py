mb_ram_per_node = 4096

mb_ram_nodig_per_vm = {
    "alpine": 20,
    "ubuntu": 80,
    "centos": 100,
    "windows": 800
}

aantal_vms_per_server = {
    "node_01": {
        "alpine": 10,
        "ubuntu": 3,
        "windows": 2
    },
    "node_02": {
        "alpine": 42,
        "ubuntu": 9
    },
    "node_03": {
        "windows": 4
    },
}


def belast_in_mb(alpine: int = 0, ubuntu: int = 0, 
                   centos: int = 0, windows: int = 0) -> int:
    """_summary_

    Args:
        alpine (int, optional): alpine. Defaults to 0.
        ubuntu (int, optional): ubuntu. Defaults to 0.
        centos (int, optional): centos. Defaults to 0.
        windows (int, optional):windows. Defaults to 0.

    Returns:
        int: totaal aantal ram geheugen
    """

    totaal_mb = 0
    totaal_mb += mb_ram_nodig_per_vm["alpine"] * alpine
    totaal_mb += mb_ram_nodig_per_vm["ubuntu"] * ubuntu
    totaal_mb += mb_ram_nodig_per_vm["centos"] * centos
    totaal_mb += mb_ram_nodig_per_vm["windows"] * windows

    return totaal_mb
    """ server = "node_01"
    print(belast_in_mb(**aantal_vms_per_server)[server]) """

def belast_totaal_ram(server: str) -> int:
    """Berekent het totaal aantal ram.

    Args:
        server (str): de naam van de server

    Returns:
        int: het totaal aantal belast vms
    """
    return sum(aantal_vms_per_server[server].values())


def welke_server() -> str:
    """Vraagt welke server

    Returns:
        str: naam van de server of lege string
    """
    naam_servers = aantal_vms_per_server.keys()
    server = None

    while (server not in naam_servers) and (server != ""):
        server = input("Welke server? : ")

    return server

def prct (num):
    prct = float(num)/ float(4096)* 100
    return int(prct)