from Functies import belast_in_mb, belast_totaal_ram, welke_server,prct, aantal_vms_per_server, mb_ram_nodig_per_vm

server = welke_server()

if server == "":
    servers = aantal_vms_per_server.keys()
else:
    servers = [server]

for server_naam in servers:

    aantal_mb = belast_in_mb(**aantal_vms_per_server[server_naam])
    aantal_vms= belast_totaal_ram(server_naam)

    if prct(aantal_mb) > 85 or aantal_vms > 50:
        type_server = "critical: omdat er meer dan 50 vms zijn of meer dan 85 % belast is !"
    elif prct(aantal_mb) > 70:
        type_server = "Warning: omdat het meer dan 70 % belast is!"
        
    else:
        type_server = "genoeg geheugen!"

    print(f"{server_naam} heeft in totaal: {aantal_vms} vms.  belast voor  {prct(aantal_mb)} % en is een {type_server}")
