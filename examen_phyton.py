mb_ram_per_node = 4096

mb_ram_nodig_per_vm = {
    "alpine" : 20,
    "ubuntu" : 80,
    "centos" : 100,
    "windows": 800
}

aantal_vms_per_server = {
    "node_01": {
        "alpine" : 10,
        "ubuntu" : 3,
        "windows": 2
    },
    "node_02": {
        "alpine" : 42,
        "ubuntu" : 9
    },
    "node_03": {
        "windows": 4
    },
}

def bereken_servergebruik(server_data, vm_ram_gebruik):    
    server_ram_gebruik = {}
    for server, vm_types in server_data.items():
        totale_ram_gebruik = 0
        for vm_type, vm_aantal in vm_types.items():
            totale_ram_gebruik += vm_aantal * vm_ram_gebruik[vm_type]
        server_ram_gebruik[server] = totale_ram_gebruik
    return server_ram_gebruik

def toon_servergebruik(server_ram_gebruik, totale_ram_per_server=mb_ram_per_node):
    for server, ram_gebruik in server_ram_gebruik.items():
        gebruik = (ram_gebruik / totale_ram_per_server) * 100
        if gebruik > 85:
            print(f"Server {server} is kritiek: gebruik is {gebruik:.2f}%")
            print(f"Reden: gebruik is meer dan 85%")
        elif gebruik > 70:
            print(f"Server {server} is waarschuwing: gebruik is {gebruik:.2f}%")
            print(f"Reden: gebruik is meer dan 70%")
        else:
            print(f"Server {server} gebruik is {gebruik:.2f}%")

server_ram_gebruik = bereken_servergebruik(aantal_vms_per_server, mb_ram_nodig_per_vm)
toon_servergebruik(server_ram_gebruik)
