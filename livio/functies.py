#Examen_python_Fama_Livio

mb_ram_per_node = 4096

mb_ram_nodig_per_vm = {
    "alpine": 20,
    "ubuntu": 80,
    "centos": 100,
    "windows": 800
}

aantal_vms_per_server = {
    "node_01": {
        "alpine": 10,
        "ubuntu": 3,
        "windows": 2
    },
    "node_02": {
        "alpine": 42,
        "ubuntu": 9
    },
    "node_03": {
        "windows": 4
    },
}


def hoeveel_ram(alpine: int = 0, ubuntu: int = 0, centos: int = 0, windows: int = 0) -> int:
    """Berekent het totaal ram verbruik per server

    Args:
        alpine (int, optional): aantal alpine vms . Defaults to 0.
        ubuntu (int, optional): aantal ubuntu vms . Defaults to 0.
        centos (int, optional): aantal centos vms . Defaults to 0.
        windows (int, optional):aantal windows vms . Defaults to 0.

    Returns:
        int: totaal ram in gebruik
    """
    hoeveel_ram = 0
    hoeveel_ram += mb_ram_nodig_per_vm["alpine"] * alpine
    hoeveel_ram += mb_ram_nodig_per_vm["ubuntu"] * ubuntu
    hoeveel_ram += mb_ram_nodig_per_vm["centos"] * centos
    hoeveel_ram += mb_ram_nodig_per_vm["windows"] * windows

    return hoeveel_ram



def percentage(getal:float):
    """berekent het percentage van uw ram

    Args:
        getal (float): getal dat je wil omrekenen

    Returns:
        int : het percentage getal afgerond naar boven
    """
    Percentage = float(getal)/float(4096)*100
    return int(round(Percentage))


def opvraag_server() -> str:
    """Vraagt welke server

    Returns:
        str: naam van de server of lege string
    """
    database= aantal_vms_per_server.keys()
    server = None

    while (server not in database) and (server != ""):
        server = input("Welke server wil je opvragen?: ")

    return server



def aantal_vms(server: str) -> int:
    """Berekent het totaal aantal virtual machines.

    Args:
        server (str): de naam van de server

    Returns:
        int: het totaal aantal virtual machines
    """
    return sum(aantal_vms_per_server[server].values())






