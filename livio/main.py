#Examen_python_Fama_Livio

from functies import hoeveel_ram, percentage, opvraag_server, aantal_vms, mb_ram_nodig_per_vm, aantal_vms_per_server


server_input = opvraag_server()

if server_input == "":
    servers = aantal_vms_per_server.keys()
else:
    servers = [server_input]


for server_naam in servers:

    aantal_ram = hoeveel_ram(**aantal_vms_per_server[server_naam])
    totaal_vms = aantal_vms(server_naam)

    
    if (percentage(aantal_ram)) > 85 or totaal_vms > 50:
        melding = (f" Critical !!! De server is voor {percentage(aantal_ram)} % belast.")
    
    elif (percentage(aantal_ram)) > 70 :
        melding =(f" Warning: De server is voor {percentage(aantal_ram)} % belast.")
        
    else:
        melding = (f" De server is voor {percentage(aantal_ram)} % belast.")

    print(f"Server {server_naam} heeft {totaal_vms} virtual machines draaien. De server gebruikt {aantal_ram} mb ram geheugen:{melding}")




