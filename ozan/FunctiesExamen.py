"""
Opdracht:

We hebben een overzicht van hoeveel vms (virtuele machines) draaien per server. We willen berekenen 
hoe hard een server belast is. We weten het gemiddelde ram verbruik van elk soort vm en ook hoeveel 
ram er in totaal is geinstalleerd in elke server.

Bepalingen:
 - Je moet gebruik maken van de aangeleverde dictionaries
 - Je mag deze dictionaries niet aanpassen
 - Het is de bedoeling dat je op het einde 1 programma hebt
 - Inlever datum is maandag ochtend 6 februari 2023, ten laatste 09:00 CET

/15ptn  1 - Bereken hoeveel ram in gebruik is op elke server
/ 5ptn  2 - Maak gebruik van output met f strings om dit weer te geven
/15ptn  3 - Maak gebruik van functies voor logica die je wenst te hergebruiken
/10ptn  4 - Roep deze functies aan met unpacking (* of **)
/ 5ptn  5 - Voorzie alle functies van een docstring
/ 5ptn  6 - Verwerk deze functies in een aparte file en importeer deze
/15ptn  7 - Vraag met input voor welke server je de  berekening wenst. Indien er niets 
            wordt ingevuld laat je standaard de belasting voor elke server zien. Tip: (dict.keys())
/15ptn  8 - Indien een server voor meer dan 70% belast is vermeld je in je output een "warning".
            Vermeld de reden ook in je output.
/15ptn  9 - Indien een server voor meer dan 85% belast is of in totaal meer dan 50 vms heeft 
            draaien vermeld je in je output dat deze server "critical" is. Vermeld in de reden 
            ook in je output.

Totaal  /100ptn
"""

mb_ram_per_node = 4096

mb_ram_nodig_per_vm = {
    "alpine" : 20,
    "ubuntu" : 80,
    "centos" : 100,
    "windows": 800
}

aantal_vms_per_server = {
    "node_01": {
        "alpine" : 10,
        "ubuntu" : 3,
        "windows": 2
    },
    "node_02": {
        "alpine" : 42,
        "ubuntu" : 9
    },
    "node_03": {
        "windows": 4
    },
}




def gebruik_ram(alpine: int = 0, ubuntu: int = 0, centos: int = 0, windows: int = 0):
    """
    berekent hoeveel ram er in gebruik is.

    Args:
        alpine (int, optional): aantal mb ram. Defaults to 0.
        ubuntu (int, optional): aantal mb ram. Defaults to 0.
        centos (int, optional): aantal mb ram. Defaults to 0.
        windows (int, optional): aantal mb ram. Defaults to 0.

    Returns:
        int: totaal gebruik ram
    """
    gebruik_ram = 0
    
    gebruik_ram_alpine = mb_ram_nodig_per_vm["alpine"] * alpine
    gebruik_ram_ubuntu = mb_ram_nodig_per_vm["ubuntu"] * ubuntu
    gebruik_ram_centos = mb_ram_nodig_per_vm["centos"] * centos
    gebruik_ram_windows = mb_ram_nodig_per_vm["windows"] * windows

    gebruik_ram = gebruik_ram_alpine + gebruik_ram_ubuntu + gebruik_ram_centos + gebruik_ram_windows

    return gebruik_ram




def aantal_gebruik_vms(machine: str) -> int:
    """
    berekent aantal virtual machines

    Args:
        machine (str): naam van server

    Returns:
        int: totaal aantal virtual machines
    """


    return sum (aantal_vms_per_server[machine].values())



def welke_node():
    """
    vraagt welke Node

    Returns:
        str: naam van de Node of niks
    """
    
    machine = input("Welke Node? ")
    while machine not in aantal_vms_per_server.keys() and machine != "":
        
        machine = input("Welke Node? ")
    
    return machine





def percentage (getal:float):
    """
    berekent percentage ram

    Args:
        getal (float): percentage dat je wilt berekenen

    Returns:
        _type_: percentage afgerond
    """

    Percentage = float(getal)/mb_ram_per_node*100
    
    
    return int(round(Percentage))

























#node_01 = "node_01"

#node_02 = "node_02"

#node_03 = "node_03"

#print (gebruik_ram(**aantal_vms_per_server["node_01"]))

#print (gebruik_ram(**aantal_vms_per_server["node_02"]))

#print (gebruik_ram(**aantal_vms_per_server["node_03"])