"""
Opdracht:

We hebben een overzicht van hoeveel vms (virtuele machines) draaien per server. We willen berekenen 
hoe hard een server belast is. We weten het gemiddelde ram verbruik van elk soort vm en ook hoeveel 
ram er in totaal is geinstalleerd in elke server.

Bepalingen:
 - Je moet gebruik maken van de aangeleverde dictionaries
 - Je mag deze dictionaries niet aanpassen
 - Het is de bedoeling dat je op het einde 1 programma hebt
 - Inlever datum is maandag ochtend 6 februari 2023, ten laatste 09:00 CET

/15ptn  1 - Bereken hoeveel ram in gebruik is op elke server
/ 5ptn  2 - Maak gebruik van output met f strings om dit weer te geven
/15ptn  3 - Maak gebruik van functies voor logica die je wenst te hergebruiken
/10ptn  4 - Roep deze functies aan met unpacking (* of **)
/ 5ptn  5 - Voorzie alle functies van een docstring
/ 5ptn  6 - Verwerk deze functies in een aparte file en importeer deze
/15ptn  7 - Vraag met input voor welke server je de  berekening wenst. Indien er niets 
            wordt ingevuld laat je standaard de belasting voor elke server zien. Tip: (dict.keys())
/15ptn  8 - Indien een server voor meer dan 70% belast is vermeld je in je output een "warning".
            Vermeld de reden ook in je output.
/15ptn  9 - Indien een server voor meer dan 85% belast is of in totaal meer dan 50 vms heeft 
            draaien vermeld je in je output dat deze server "critical" is. Vermeld in de reden 
            ook in je output.

Totaal  /100ptn
"""

mb_ram_per_node = 4096

mb_ram_nodig_per_vm = {
    "alpine" : 20,
    "ubuntu" : 80,
    "centos" : 100,
    "windows": 800
}

aantal_vms_per_server = {
    "node_01": {
        "alpine" : 10,
        "ubuntu" : 3,
        "windows": 2
    },
    "node_02": {
        "alpine" : 42,
        "ubuntu" : 9
    },
    "node_03": {
        "windows": 4
    },
}



from FunctiesExamen import gebruik_ram, aantal_gebruik_vms, welke_node, percentage, aantal_vms_per_server


node_input = welke_node()

if node_input == "":
    vm_machines = aantal_vms_per_server.keys()

else:
    vm_machines = [node_input]

for node_naam in vm_machines:

    aantal_ram = gebruik_ram(**aantal_vms_per_server[node_naam])
    
    aantal_vms = aantal_gebruik_vms(node_naam)

    if (percentage(aantal_ram)) > 85 or aantal_vms > 50:
        fout_melding = (f"Critical warning server is voor {percentage(aantal_ram)} % belast")

    elif (percentage(aantal_ram)) > 70 :
        fout_melding =(f" warning server is voor {percentage(aantal_ram)} % belast.")

    else:
        fout_melding =(f"server is voor {percentage(aantal_ram)} % belast")

    print(f"server{node_naam} heeft {aantal_vms} virtueel machines draaien. server node_001gebruikt {aantal_ram} mb ram geheugen : {fout_melding}")




