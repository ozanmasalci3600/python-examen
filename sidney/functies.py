mb_ram_per_node = 4096

mb_ram_nodig_per_vm = {
    "alpine": 20,
    "ubuntu": 80,
    "centos": 100,
    "windows": 800
}

aantal_vms_per_server = {
    "node_01": {
        "alpine": 10,
        "ubuntu": 3,
        "windows": 2
    },
    "node_02": {
        "alpine": 42,
        "ubuntu": 9
    },
    "node_03": {
        "windows": 4
    },
}


def totaal_ram_gebruikt(alpine: int = 0, ubuntu: int = 0, centos: int = 0, windows: int = 0) -> int:
    """
    Geeft totaal ram gebruik per node.
    Args:
        alpine (int, optional): aantal vms alpine. Defaults to 0.
        ubuntu (int, optional): aantal vms ubuntu. Defaults to 0.
        centos (int, optional): aantal vms centos. Defaults to 0.
        windows (int, optional): aantal vms windows. Defaults to 0.

    Returns:
        int: Totaal ram gebruikt.
    """

    totaal_ram = 0

    totaal_ram += mb_ram_nodig_per_vm["alpine"] * alpine
    totaal_ram += mb_ram_nodig_per_vm["ubuntu"] * ubuntu
    totaal_ram += mb_ram_nodig_per_vm["centos"] * centos
    totaal_ram += mb_ram_nodig_per_vm["windows"] * windows

    return totaal_ram


def totaal_aantal_vms(node: str) -> int:
    """
    Geeft totaal aantal vms. 

    Returns:
        int: Het totaal aantal vms.
    """

    return sum(aantal_vms_per_server[node].values())


def welke_node() -> str:
    """
    Vraagt welke node.

    Returns:
        str: Naam van de node of lege string
    """

    gekende_nodes = aantal_vms_per_server.keys()
    node = None

    while (node not in gekende_nodes) and (node != ""):
        node = input("Welke node? ")

    return node
