"""
Opdracht:

We hebben een overzicht van hoeveel vms (virtuele machines) draaien per server. We willen berekenen 
hoe hard een server belast is. We weten het gemiddelde ram verbruik van elk soort vm en ook hoeveel 
ram er in totaal is geinstalleerd in elke server.

Bepalingen:
 - Je moet gebruik maken van de aangeleverde dictionaries
 - Je mag deze dictionaries niet aanpassen
 - Het is de bedoeling dat je op het einde 1 programma hebt
 - Inlever datum is maandag ochtend 6 februari 2023, ten laatste 09:00 CET

/15ptn  1 - Bereken hoeveel ram in gebruik is op elke server
/ 5ptn  2 - Maak gebruik van output met f strings om dit weer te geven
/15ptn  3 - Maak gebruik van functies voor logica die je wenst te hergebruiken
/10ptn  4 - Roep deze functies aan met unpacking (* of **)
/ 5ptn  5 - Voorzie alle functies van een docstring
/ 5ptn  6 - Verwerk deze functies in een aparte file en importeer deze
/15ptn  7 - Vraag met input voor welke server je de  berekening wenst. Indien er niets 
            wordt ingevuld laat je standaard de belasting voor elke server zien. Tip: (dict.keys())
/15ptn  8 - Indien een server voor meer dan 70% belast is vermeld je in je output een "warning".
            Vermeld de reden ook in je output.
/15ptn  9 - Indien een server voor meer dan 85% belast is of in totaal meer dan 50 vms heeft 
            draaien vermeld je in je output dat deze server "critical" is. Vermeld in de reden 
            ook in je output.

Totaal  /100ptn
"""

mb_ram_per_node = 4096

mb_ram_nodig_per_vm = {
    "alpine": 20,
    "ubuntu": 80,
    "centos": 100,
    "windows": 800
}

aantal_vms_per_server = {
    "node_01": {
        "alpine": 10,
        "ubuntu": 3,
        "windows": 2
    },
    "node_02": {
        "alpine": 42,
        "ubuntu": 9
    },
    "node_03": {
        "windows": 4
    },
}

from functies import totaal_ram_gebruikt, totaal_aantal_vms, welke_node, aantal_vms_per_server


node_input = welke_node()

if node_input == "":
    nodes = aantal_vms_per_server.keys()
else:
    nodes = [node_input]

for node_naam in nodes:    

    aantal_vms = totaal_ram_gebruikt(**aantal_vms_per_server[node_naam])

    aantal_nodes = totaal_aantal_vms(node_naam)


    if aantal_vms > 0.85 * mb_ram_per_node or aantal_nodes > 50:
        node_belasting = "Critical !!"
    elif aantal_vms > 0.7 * mb_ram_per_node:
        node_belasting = "Warning !"
    else:
        node_belasting = "OK"

    print(f"Server '{node_naam}' heeft {aantal_nodes} VM's en {aantal_vms}MB ram in gebruik. Healthcheck = {node_belasting}")
