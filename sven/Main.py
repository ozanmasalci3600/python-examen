"""
Opdracht:

We hebben een overzicht van hoeveel vms (virtuele machines) draaien per server. We willen berekenen 
hoe hard een server belast is. We weten het gemiddelde ram verbruik van elk soort vm en ook hoeveel 
ram er in totaal is geinstalleerd in elke server.

Bepalingen:
 - Je moet gebruik maken van de aangeleverde dictionaries
 - Je mag deze dictionaries niet aanpassen
 - Het is de bedoeling dat je op het einde 1 programma hebt
 - Inlever datum is maandag ochtend 6 februari 2023, ten laatste 09:00 CET

/15ptn  1 - Bereken hoeveel ram in gebruik is op elke server
/ 5ptn  2 - Maak gebruik van output met f strings om dit weer te geven
/15ptn  3 - Maak gebruik van functies voor logica die je wenst te hergebruiken
/10ptn  4 - Roep deze functies aan met unpacking (* of **)
/ 5ptn  5 - Voorzie alle functies van een docstring
/ 5ptn  6 - Verwerk deze functies in een aparte file en importeer deze
/15ptn  7 - Vraag met input voor welke server je de  berekening wenst. Indien er niets 
            wordt ingevuld laat je standaard de belasting voor elke server zien. Tip: (dict.keys())
/15ptn  8 - Indien een server voor meer dan 70% belast is vermeld je in je output een "warning".
            Vermeld de reden ook in je output.
/15ptn  9 - Indien een server voor meer dan 85% belast is of in totaal meer dan 50 vms heeft 
            draaien vermeld je in je output dat deze server "critical" is. Vermeld in de reden 
            ook in je output.

Totaal  /100ptn
"""

from functions import hoeveelheid_ram_in_mb, aantal_vms, welke_server



mb_ram_per_node = 4096

mb_ram_nodig_per_vm = {
    "alpine" : 20,
    "ubuntu" : 80,
    "centos" : 100,
    "windows": 800
}

aantal_vms_per_server = {
    "node_01": {
        "alpine" : 10,
        "ubuntu" : 3,
        "windows": 2
    },
    "node_02": {
        "alpine" : 42,
        "ubuntu" : 9
    },
    "node_03": {
        "windows": 4
    },
} 
           
       
       
server_input = welke_server()

if server_input == "":
    servers= aantal_vms_per_server.keys()
else:
    servers= [server_input]
    
for server_name in servers:    
    
    uitkomst_ram = hoeveelheid_ram_in_mb(**aantal_vms_per_server[server_name])
    uitkomst_vms = aantal_vms(server_name)

    type_belasting= "ok server"

    if uitkomst_ram > 2867:
        type_belasting= "Warning."
    elif uitkomst_ram > 3482 or uitkomst_vms > 50:
        type_belasting= "Critical!"
    else:
        type_belasting= ""    

    print(f"{server_name} beschikt over {uitkomst_vms} vms met een ram verbruik van {uitkomst_ram}MB. {type_belasting}")


