mb_ram_per_node = 4096

mb_ram_nodig_per_vm = {
    "alpine" : 20,
    "ubuntu" : 80,
    "centos" : 100,
    "windows": 800
}

aantal_vms_per_server = {
    "node_01": {
        "alpine" : 10,
        "ubuntu" : 3,
        "windows": 2
    },
    "node_02": {
        "alpine" : 42,
        "ubuntu" : 9
    },
    "node_03": {
        "windows": 4
    },
} 


def hoeveelheid_ram_in_mb(alpine:int = 0, ubuntu:int = 0, centos:int = 0, windows:int = 0) -> int:
    """

    Args:
        alpine (int, optional): aantal ram verbruik in mb per vm alpine maal verbruik specifieke vm alpine. Defaults to 0.
        ubuntu (int, optional): aantal ram verbruik in mb per vm ubuntu maal verbruik specifieke vm ubuntu. Defaults to 0.
        centos (int, optional): aantal ram verbruik in mb per vm centos maal verbruik specifieke vm centos. Defaults to 0.
        windows (int, optional): aantal ram verbruik in mb per vm windows maal verbruik specifieke vm windows. Defaults to 0.

    Returns:
        int: Totaal ram verbruik in mb per server.
    """


    totaal_ram = 0
   
    
    totaal_ram += mb_ram_nodig_per_vm ["alpine"] * alpine
    totaal_ram += mb_ram_nodig_per_vm ["ubuntu"] * ubuntu
    totaal_ram += mb_ram_nodig_per_vm ["centos"] * centos
    totaal_ram += mb_ram_nodig_per_vm ["windows"] * windows
   
   
    return totaal_ram 
    
 
        
def aantal_vms(server:str)->int:
    """Bereken het aantal vms in server.

    Args:
        server (str): De naam van server.

    Returns:
        int: Totaal aantal vms.
    """
    return sum(aantal_vms_per_server[server].values())
    

    

def welke_server()->str:
    """Vraag voor welke server.

    Returns:
        str: Naam van de server, indien niet gekend antwoord, leeg.
    """
    
    wetende_servers = aantal_vms_per_server.keys()
    server = None
    
    while (server not in wetende_servers) and (server != ""):
      server = input("Welke server?")
      
    return server