mb_ram_per_node = 4096

mb_ram_nodig_per_vm = {
    "alpine" : 20,
    "ubuntu" : 80,
    "centos" : 100,
    "windows": 800
}                                                          

aantal_vms_per_server = {
    "node_01": {
        "alpine" : 10,
        "ubuntu" : 3,
        "windows": 2
    },
    "node_02": {
        "alpine" : 42,
        "ubuntu" : 9
    },
    "node_03": {
        "windows": 4
    },
}

def ram_per_node (alpine: int=0, ubuntu: int=0, centos: int=0, windows: int=0) -> int:
   
   
    """
    ram geheugen per vm  
    Args:
        alpine (int, optional): alpine Defaults to 0.
        ubuntu (int, optional): ubuntu Defaults to 0.
        centos (int, optional): centos Defaults to 0.
        windows (int, optional):windows Defaults to 0.

    Returns:
        int: sum_mb
    """
    sum_mb =0
    sum_mb += mb_ram_nodig_per_vm["alpine"]  * alpine
    sum_mb += mb_ram_nodig_per_vm["ubuntu"]  * ubuntu
    sum_mb += mb_ram_nodig_per_vm["centos"]  * centos
    sum_mb += mb_ram_nodig_per_vm["windows"]  * windows
    
  
    return sum_mb


def aantal_ram (node: str) -> int:
    """aantal_ram

    Args:
        (node: str) -> int: 

    Returns: sum(aantal_vms_per_server)[node].values()
        
    """
    return sum(aantal_vms_per_server)[node].values()


def welke_node() -> str:
    
    """welke_node

    Returns: node
    """
    
    naam_nodes = aantal_vms_per_server.keys()
    node = None
    
    while (node not in naam_nodes) and  (node != ""):
        node = input("Welke node? ")
    
    return node

    


    