mb_ram_per_node = 4096

mb_ram_nodig_per_vm = {
    "alpine" : 20,
    "ubuntu" : 80,
    "centos" : 100,
    "windows": 800
}

aantal_vms_per_server = {
    "node_01": {
        "alpine" : 10,
        "ubuntu" : 3,
        "windows": 2
    },
    "node_02": {
        "alpine" : 42,
        "ubuntu" : 9
    },
    "node_03": {
        "windows": 4
    },
}


def belasting_node(alpine: int=0,ubuntu: int=0,centos: int=0,windows: int=0) -> int:
    """Berekend het totaal mb dat de node gebruikt

    Args:
        alpine (int, optional):aantal vms alpine  Defaults to 0.
        ubuntu (int, optional):aantal vms ubuntu  Defaults to 0.
        centos (int, optional):aantal vms centos  Defaults to 0.
        windows (int, optional):aantal vms windows  Defaults to 0.

    Returns:
        int: totaal gebruik van de node
    """
    totaal_server = 0

    totaal_server += mb_ram_nodig_per_vm["alpine"] * alpine
    totaal_server += mb_ram_nodig_per_vm["ubuntu"] * ubuntu
    totaal_server += mb_ram_nodig_per_vm["centos"] * centos
    totaal_server += mb_ram_nodig_per_vm["windows"] * windows

    return totaal_server


def totaal_vms(node:str) -> int:
    """berekend hoeveel vms er zijn.

    Args:
        node (str): de naam van de node

    Returns:
        int: aantal vms dat er in die node zitten
    """
    return sum(aantal_vms_per_server[node].values())


def welke_node() -> str:
    """vraagt welke node

    Returns:
        str: naam van de node of een lege string
    """
    gekende_nodes = aantal_vms_per_server.keys()
    node = None

    while (node not in gekende_nodes) and (node != ""):
        node = input("welke node wil je hebben? ")
   
    return node