"""
Opdracht:

We hebben een overzicht van hoeveel vms (virtuele machines) draaien per server. We willen berekenen 
hoe hard een server belast is. We weten het gemiddelde ram verbruik van elk soort vm en ook hoeveel 
ram er in totaal is geinstalleerd in elke server.

Bepalingen:
 - Je moet gebruik maken van de aangeleverde dictionaries
 - Je mag deze dictionaries niet aanpassen
 - Het is de bedoeling dat je op het einde 1 programma hebt
 - Inlever datum is maandag ochtend 6 februari 2023, ten laatste 09:00 CET

/15ptn  1 - Bereken hoeveel ram in gebruik is op elke server
/ 5ptn  2 - Maak gebruik van output met f strings om dit weer te geven
/15ptn  3 - Maak gebruik van functies voor logica die je wenst te hergebruiken
/10ptn  4 - Roep deze functies aan met unpacking (* of **)
/ 5ptn  5 - Voorzie alle functies van een docstring
/ 5ptn  6 - Verwerk deze functies in een aparte file en importeer deze
/15ptn  7 - Vraag met input voor welke server je de  berekening wenst. Indien er niets 
            wordt ingevuld laat je standaard de belasting voor elke server zien. Tip: (dict.keys())
/15ptn  8 - Indien een server voor meer dan 70% belast is vermeld je in je output een "warning".
            Vermeld de reden ook in je output.
/15ptn  9 - Indien een server voor meer dan 85% belast is of in totaal meer dan 50 vms heeft 
            draaien vermeld je in je output dat deze server "critical" is. Vermeld in de reden 
            ook in je output.

Totaal  /100ptn
"""

from functies import belasting_node, welke_node, totaal_vms, aantal_vms_per_server


node_input = welke_node()

if node_input == "":
    nodes = aantal_vms_per_server.keys()
else:
    nodes = [node_input]

for node_naam in nodes:

    totaal_belasting = belasting_node(**aantal_vms_per_server[node_naam])
    totaal_percent= totaal_vms(node_naam)
    percentage_berekenen = round(totaal_belasting/int(4096)*100)


    if percentage_berekenen > 85:
        type_percent = '"critical" de node is voor meer dan 85% belast'
    elif percentage_berekenen > 70:
        type_percent = '"warning" de node is voor meer dan 70% belast'
    else:
        type_percent= ""


    print(f"{node_naam} gebruikt {totaal_belasting} mb en is voor {percentage_berekenen}% belast {type_percent} ")

