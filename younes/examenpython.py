"""
Opdracht:

We hebben een overzicht van hoeveel vms (virtuele machines) draaien per server. We willen berekenen 
hoe hard een server belast is. We weten het gemiddelde ram verbruik van elk soort vm en ook hoeveel 
ram er in totaal is geinstalleerd in elke server.

Bepalingen:
 - Je moet gebruik maken van de aangeleverde dictionaries
 - Je mag deze dictionaries niet aanpassen
 - Het is de bedoeling dat je op het einde 1 programma hebt
 - Inlever datum is maandag ochtend 6 februari 2023, ten laatste 09:00 CET

/15ptn  1 - Bereken hoeveel ram in gebruik is op elke server
/ 5ptn  2 - Maak gebruik van output met f strings om dit weer te geven
/15ptn  3 - Maak gebruik van functies voor logica die je wenst te hergebruiken
/10ptn  4 - Roep deze functies aan met unpacking (* of **)
/ 5ptn  5 - Voorzie alle functies van een docstring
/ 5ptn  6 - Verwerk deze functies in een aparte file en importeer deze
/15ptn  7 - Vraag met input voor welke server je de  berekening wenst. Indien er niets 
            wordt ingevuld laat je standaard de belasting voor elke server zien. Tip: (dict.keys())
/15ptn  8 - Indien een server voor meer dan 70% belast is vermeld je in je output een "warning".
            Vermeld de reden ook in je output.
/15ptn  9 - Indien een server voor meer dan 85% belast is of in totaal meer dan 50 vms heeft 
            draaien vermeld je in je output dat deze server "critical" is. Vermeld in de reden 
            ook in je output.

Totaal  /100ptn
"""
from functies_examen import gebruik_in_ram, aantal_totaal_vms, welke_server, aantal_vms_per_server, mb_ram_per_node

server_input = welke_server()

if server_input == "":
    servers = aantal_vms_per_server.keys()
else:
    servers = [server_input]

for server_naam in servers:
    aantal_mb_in_ram = gebruik_in_ram(**aantal_vms_per_server[server_naam])
    aantal_vms = aantal_totaal_vms(server_naam)
    
    percentage_belast_server = aantal_mb_in_ram/mb_ram_per_node*100

    if percentage_belast_server > 85 or aantal_vms > 50:
        output_server = "critical"
        print(f"{server_naam} heeft in totaal {aantal_vms} vms en gebruikt in totaal {aantal_mb_in_ram} mb in ram! De server is {output_server} omdat de server meer dan 85% belast is OF/EN meer dan 50vms heeft!!! ")
    elif percentage_belast_server > 70:
        output_server = "Warning"
        print(f"{server_naam} heeft in totaal {aantal_vms} vms en gebruikt in totaal {aantal_mb_in_ram} mb in ram! {output_server} omdat de server meer dan 75% belast is!!! ")
    else:
        output_server = "good"
        print(f"{server_naam} heeft in totaal {aantal_vms} vms en gebruikt in totaal {aantal_mb_in_ram} mb in ram! De server is {output_server} omdat het niet belast is! ")

    
