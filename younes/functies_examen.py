mb_ram_per_node = 4096

mb_ram_nodig_per_vm = {
    "alpine": 20,
    "ubuntu": 80,
    "centos": 100,
    "windows": 800
}

aantal_vms_per_server = {
    "node_01": {
        "alpine": 10,
        "ubuntu": 3,
        "windows": 2
    },
    "node_02": {
        "alpine": 42,
        "ubuntu": 9
    },
    "node_03": {
        "windows": 4
    },
}


def gebruik_in_ram(alpine: int = 0, ubuntu: int = 0, centos: int = 0, windows: int = 0) -> int:
    """Berekend het totaal gebruik mb in ram dat de server heeft gebruikt

    Args:
        alpine (int, optional): aantal mb ram alpine. Defaults to 0.
        ubuntu (int, optional): aantal mb ram ubuntu. Defaults to 0.
        centos (int, optional): aantal mb ram centos. Defaults to 0.
        windows (int, optional): aantal mb ram windows. Defaults to 0.

    Returns:
        int: totaal aantal mb in ram gebruikt
    """
    totaal_ram = 0

    totaal_ram += mb_ram_nodig_per_vm["alpine"] * alpine
    totaal_ram += mb_ram_nodig_per_vm["ubuntu"] * ubuntu
    totaal_ram += mb_ram_nodig_per_vm["centos"] * centos
    totaal_ram += mb_ram_nodig_per_vm["windows"] * windows

    return totaal_ram


def aantal_totaal_vms(server: str) -> int:
    """berekend het totaal vms per server

    Args:
        server (str): de server

    Returns:
        int: totaal aantal vms per server
    """
    return sum(aantal_vms_per_server[server].values())


def welke_server() -> str:
    """Vraagt welke server

    Returns:
        str: naam van de server of een lege string
    """
    gekende_servers = aantal_vms_per_server.keys()
    server = None

    while (server not in gekende_servers) and (server != ""):
        server = input("Welke server?")
    return server
